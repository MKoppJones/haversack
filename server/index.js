const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config');
const db = require('./db');

let http = null;
http = require('http').Server(app);

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/monsters', async (req, res) => {
  const connection = await db.syncConnection();
  const monsters = await connection.execute(
    'SELECT * FROM Monsters ORDER BY Name'
  );
  res.status(200).send(monsters[0]);
});

http.listen(config.PORT, config.HOST, () => {
  console.log(`listening on ${config.HOST}:${config.PORT}`);
});