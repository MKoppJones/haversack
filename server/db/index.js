const mysql = require('mysql2');
const mysqlp = require('mysql2/promise');
const config = require('../config');

let syncConnection;

async function handlePromiseDisconnect() {
  syncConnection = await mysqlp.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    database: process.env.DB,
    password: process.env.DBPWD,
  });
  syncConnection.connect(async (err) => {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handlePromiseDisconnect, 2000);
      // We introduce a delay before attempting to reconnect,
    } else {
      console.log('connected to db sync');
    }
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  syncConnection.on('error', async (err) => {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      await handlePromiseDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

const db = {
  syncConnection: async () => {
    if (!syncConnection) {
      await handlePromiseDisconnect();
    }
    return syncConnection;
  },
};

module.exports = db;
