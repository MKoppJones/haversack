const Joi = require('joi');
require('dotenv').config();

// Validate
const schema = Joi.object().keys({
  PORT: Joi.string().required().default(3000),
  HOST: Joi.string().required().default('localhost'),
  DBHOST: Joi.string().required(),
  DB: Joi.string().required(),
  DBUSER: Joi.string().required(),
  DBPWD: Joi.string().required(),
}).unknown(true);

const { error, value: config } = Joi.validate(process.env, schema);

if (error) {
  console.error('Missing property in config.', error.message);
  process.exit(1);
}

module.exports = config;
