var fs = require('fs');
fs.readFile('output.json', 'utf8', function (err, data) {
  if (err) throw err;
  const obj = JSON.parse(data);
  let output = '';
  // const output = {};
  for (let key in obj) {
    const item = obj[key];
    const out = `INSERT INTO Monsters (Name, CR, Size, Type, Source, Environment, Alignment) VALUES ('${item.Name.replace(`'`,'\\\'')}','${item.CR}','${item.Size}','${item.Type}','${item.Source}','${JSON.stringify(item.Environment) || '{}'}', '${item.Alignment || 'Any'}');`;
    output += `${out}\n`;
    // const out = {
    //   Name: item.name,
    //   CR: item.challenge,
    //   Size: item.size,
    //   Type: item.type,
    //   Source: item.page,
    //   Environment: item.environment,
    //   Alignment: item.alignment
    // }
    // output[item.name] = out;
  }
  fs.writeFile("output.txt", output, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
  });
});