import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    masterUri: 'http://86.2.165.252:8081',
  },
  mutations: {

  },
  actions: {

  },
});
