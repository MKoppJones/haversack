import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/races',
      name: 'races',
      component: () => import('./views/Races'),
    },
    {
      path: '/classes',
      name: 'classes',
      component: () => import('./views/Classes'),
    },
    {
      path: '/classes/druid',
      name: 'druid',
      component: () => import('./views/Druid'),
    },
    {
      path: '/monsters',
      name: 'monsters',
      component: () => import('./views/Monsters'),
    },
    {
      path: '/spells',
      name: 'spells',
      component: () => import('./views/Spells'),
    },
    {
      path: '/magic-items',
      name: 'magic-items',
      component: () => import('./views/MagicItems'),
    },
    {
      path: '/crafting',
      name: 'crafting',
      component: () => import('./views/Crafting'),
    },
    {
      path: '/encounter-calculator',
      name: 'encounter-calculator',
      component: () => import('./views/EncounterCalculator'),
    },
    {
      path: '/character-creator',
      name: 'character-creator',
      component: () => import('./views/CharacterCreator'),
    },
  ],
});
