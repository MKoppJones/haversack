import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App';
import router from './router';
import store from './store';
import 'vuetify/dist/vuetify.min.css';

const request = require('request');

Vue.use(Vuetify, {
  iconfont: 'fa',
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  data: {
    get(uri, cb) {
      request.get({
        uri: `${store.state.masterUri}${uri}`,
      },
      cb);
    },
  },
}).$mount('#app');
